var customerData;
var invoiceData;
var productData;

$(document).ready(function() 
{
    $.getJSON({
        url: "customer.json",
        mimeType: "application/json",
        success: parseCustomerJSON
    });
    $.getJSON({
        url: "product.json",
        mimeType: "application/json",
        success: parseProductJSON
    });
    $.getJSON({
        url: "invoice.json",
        mimeType: "application/json",
        success: parseInvoiceJSON
    });
    $(":mobile-pagecontainer").on("pagecontainerbeforetransition", function (event, ui) 
    {
        $("#header").prependTo(ui.toPage);
    });
    $(document).on("pagecreate", function(event)
    {
        // page is an invoice page
        if (event.target.id.includes("invoice"))
        {
            loadInvoice(event.target.id.replace("invoice+", ""));
        }
    });
});
// CUSTOMER
function parseCustomerJSON(data) 
{
    customerData = data.customers;
    for (var i=0; i < customerData.length; i++) 
    {
        $("#customer").append(appendCustomer(customerData[i], i));
        $("#customer").trigger('create');

    };
    $("#customer").append("<div id='allcustomersmap' style='width:100%; height:500px'></div>");
    drawAllCustomersOnMap();
}

function appendCustomer(customerData, id)
{
    var customerName = customerData.compName;
    var customerAddress = customerData.compAddr;
    var customerContact = customerData.compContact;
    var customerPhone = customerData.compPhone;
    var customerEmail = customerData.compEmail;
    
    var customer = $("<div id='ci" + id + "' data-role='collapsible'>");
    customer.append("<h1>" + customerName + "</h1>");
    customer.append("<p> Adress: " + customerAddress + "</p>");
    customer.append("<p> Contact Name: " + customerContact + "</p>");
    customer.append("<p> Company Phone: " + customerPhone + "</p>");
    customer.append("<a href='mailto:" + customerEmail + "'> Email: " + customerEmail + "</a>");
    customer.append("<div id='map" + id + "' style='width:100%; height:500px'></div>");

    // draw the map when they expand the customer
    $(document).on("collapsibleexpand", "#ci" + id,  clickEvent(customerAddress, customerName, id));

    customer.append(appendInvoice(customerData, id));
    return customer;
}

function appendInvoice(customerData, id)
{
    var inv = $("<div id='inv" + id + "'>").append("<h2>Invoices</h2>");

    for (var j = 0; j < customerData.invoices.length; j++) 
    {
        inv.append("<a href='#invoice+" + customerData.invoices[j] + "'>" + customerData.invoices[j] + "</a><br>");
    }
    inv.collapsible();
    return inv;
}

// INVOICE
function parseInvoiceJSON(data) 
{
    invoiceData = data.invoices;
    loadEmptyInvoicePages();
}

function loadEmptyInvoicePages()
{
    for (var i = 0; i < invoiceData.length; i++)
    {
        var invoiceNumber = invoiceData[i].invNum;
        $("body").append(
            "<div id='invoice+" + invoiceNumber + "' data-role='page' class='center'>"+
                "<div id='main" + invoiceNumber + "' role='main' class='ui-content center'>"+
                    "<h1 class='center' data-role='content'>Invoice (" + invoiceNumber + ")</h1>"+
                    // MAIN CONTENT WILL BE INSERTED HERE ON PAGELOAD
                "</div>"+
            "</div>");
    }
}

function loadInvoice(id)
{
    // get the actual invoice
    var invoice = getInvoice(id);

    var invoiceNumber = invoice.invNum;
    var invoiceDate = invoice.invDate;
    var invoiceAmount = invoice.invAmt;
    var companyID = invoice.compID;

    $("#main" + invoiceNumber).append(
            "Invoice Date: " + invoiceDate + "<br>"+
            "Invoice Amount: $" + invoiceAmount + "<br>"+
            "Customer: " + customerData[companyID-1].compName + "<br><br>");

    
    $("#main"+invoiceNumber).append("<table id='table" + invoiceNumber + "' class='center'>"+
        "<tr>" +
            "<th>Product ID</th>"+
            "<th>Product Name</th>"+
            "<th>Product Price</th>"+
            "<th>Quantity</th>"+
            "<th>Product Total</th>"+
        "</tr>");

    $("#table" + invoiceNumber).append(getInvoiceInfo(invoice));
}

function getInvoice(id)
{
    var invoice;
    invoiceData.forEach(element => {
        if (element.invNum == id)
        {
            invoice = element;
        }
    });
    return invoice;
}

function getInvoiceInfo(invoice)
{
    var toReturn = "";
    for (var i = 0; i < invoice.products.length; i++)
    {
        var prodId = invoice.products[i].prodID;
        var prodQty = invoice.products[i].prodQty;
        var prodAmount = productData[prodId - 1].prodAmount;
        toReturn +=
            "<tr>" +
                "<th>" +prodId + "</th>"+
                "<th>" + productData[prodId - 1].prodDesc + "</th>"+
                "<th>" + prodAmount + "</th>"+
                "<th>" + prodQty + "</th>"+
                "<th>$" + prodAmount * prodQty + "</th>"+
            "</tr>";
    }
    return toReturn;
}

// PRODUCT

function parseProductJSON(data) 
{
    productData = data.products;
    for (var i=0; i < productData.length; i++) 
    {
        $("#product").append(appendProduct(productData[i], i));
        $("#product").trigger('create');

    };
}

function appendProduct(productData, id)
{
    var productName = productData.prodDesc;
    var productId = productData.prodID;
    var productAmount = productData.prodAmount;
    
    var product = $("<div id='ci" + productId + "' data-role='collapsible'>");
    product.append("<h1>" + productName + "</h1>");
    product.append("<p> Product ID: " + productId + "</p>");
    product.append("<p> Cost: $" + productAmount + "</p>");

    return product;
}


// MAPS
function drawCustomerOnMap(customerAddress, customerName, id)
{
    if(navigator.geolocation)
    {
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode
        (   
            {
                "address": customerAddress
            },
            function (results, status)
            {
                if (status == google.maps.GeocoderStatus.OK)
                {
                    lat = results[0].geometry.location.lat();
                    lng = results[0].geometry.location.lng();
                    var mapOptions = 
                    {
                        center: new google.maps.LatLng(lat, lng),
                        zoom: 14,
                        mapTypeID: 'roadmap',
                        zoomControl: false,
                        gestureHandling: 'none'
                    };
                    var map = new google.maps.Map($("#map"+id)[0], mapOptions);
                    var marker = new google.maps.Marker(
                    {
                        map: map,
                        animation: google.maps.Animation.DROP,
                        position: new google.maps.LatLng(lat, lng)
                    });
                    var info = new google.maps.InfoWindow(
                        {
                            content: customerName,
                            maxWidth: 150
                        }
                    );
                    google.maps.event.addListener(marker, "click", function()
                    {
                        info.open(map, marker)
                    });
                }
                else 
                {
                    alert("Google GEOCODESSS not working");
                }
            }
        );
    }
    else
    {
        alert("Google MAPS not working");
    }
}
function drawAllCustomersOnMap()
{
    if(navigator.geolocation)
    {
        var mapOptions = 
        {
            center: new google.maps.LatLng(43.4675, -79.6877),
            zoom: 11,
            mapTypeID: 'roadmap'
        };
        var map = new google.maps.Map($("#allcustomersmap")[0], mapOptions);
        var geocoder = new google.maps.Geocoder();
        for (var i = 0; i < customerData.length; i++)
        {
            var address = customerData[i].compAddr;
            var name = customerData[i].compName;
            geocoder.geocode
            (   
                {
                    "address": address
                },
                function (results, status)
                {
                    if (status == google.maps.GeocoderStatus.OK)
                    {
    
                        lat = results[0].geometry.location.lat();
                        lng = results[0].geometry.location.lng();      
                        new google.maps.Marker(
                        {
                            map: map,
                            animation: google.maps.Animation.DROP,
                            position: new google.maps.LatLng(lat, lng)
                        });         
                    }
                    else 
                    {
                        console.log("NOT WORKING HERE" + address);
                        alert("Google GEOCODE not working");
                    }
                }
            );
        }
    }
    else
    {
        alert("Google MAPS not working");
    }
}

function clickEvent(customerAddress, customerName, i)
{
    return function() { drawCustomerOnMap(customerAddress, customerName, i); }
}